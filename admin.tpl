
<!-- LOGIN -->
{if !empty($action) && $action == 'login'}
	<div class="fixed">
		<h2 style="text-align: left;">Увійти в Адміністративну панель</h2><br>
			<form class="rf" action="/{$lang_url}/admin/" method="POST" id="logInAdmin">
				<input type='text' name="login" placeholder="login" class="rfield" id="user_name" />
				<input type='password' name="password" placeholder="password" class="rfield" id="user_family" />
				<input type='submit'  value="Увійти" class="btn_submit disabled">
    	</form>
	</div>
{/if}
<!-- NEWS LIST-->
{if !empty($action) && $action == 'list'}

<div class="fixed adminTable">
	<br>
	<h2 style="text-align: left;">Список новин</h2><br>
	<a href='/{$lang_url}/admin/add/'>ДОДАТИ НОВИНУ</a>
        <a href='/{$lang_url}/admin/profileslist/'>ПЕРЕГДЯНУТИ ПІДОПІЧНИХ</a>
        <a href='/{$lang_url}/admin/addprofile/'>ДОДАТИ ПІДОПІЧНОГО</a>
	<br>
	<br>
	<br>
		<table border='1' width='100%'>
		<tr>
			<td><b>ID новини</b></td>
			<td><b>Назва новини</b></td>
			<td><b>Дата</b></td>
			<td><b>Керування</b></td>
		</tr>
		{foreach from=$smarty_news_list item=news}
		<tr>
			<td>{$news.id}</td>
			<td>{$news.title}</td>
			<td>{$news.published_date}</td>
			<td>
				<a href='/{$lang_url}/admin/edit/{$news.id}/'>редагувати</a> &nbsp;&nbsp;&nbsp;
				<a href='/{$lang_url}/admin/delete/{$news.id}/'>видалити</a>
			</td>
		</tr>
		{/foreach}
	</table>
	<br><br>
</div>
{/if}
<!-- ADD NEWS -->

{if !empty($action) && $action == 'add'}

	<div class="fixed">
	<br>
	<h2 style="text-align: left;">Заповніть поля</h2><br>

	<form action="/{$lang_url}/admin/submit/" method="POST" enctype=multipart/form-data>
	<table border='1' width='100%'>
		<tr>
			<td><b>Назва новини</b></td>
			<td>
				<input type="text" name="news_name" autofocus>
			</td>
		</tr>
		<tr>
			<td><b>Текст новини</b></td>
			<td>
				<textarea name="news_text"></textarea>
			</td>
		</tr>
                <tr>
			<td><b>Відео новини (необов'язково)</b></td>
			<td>
				<textarea name="news_video"></textarea>
			</td>
		</tr>
		<tr>
			<td><b>Додати зображення</b></td>
			<td>
				<div class="file-upload">
					<button type="button">Завантажити файл</button>
					<input type="file" name="uploadfile">
				</div>
				<input type="text" id="filename" class="filename" disabled>
			</td>
		</tr>
                		<tr>
			<td><b>Додаткове зображення №2</b></td>
			<td>
				<div class="file-upload">
					<button type="button">Завантажити файл2</button>
					<input type="file" name="uploadfile2">
				</div>
				<input type="text" id="filename2" class="filename" disabled>
			</td>
		</tr>
                		<tr>
			<td><b>Додаткове зображення №3</b></td>
			<td>
				<div class="file-upload">
					<button type="button">Завантажити файл3</button>
					<input type="file" name="uploadfile3">
				</div>
				<input type="text" id="filename3" class="filename" disabled>
			</td>
		</tr>
                		<tr>
			<td><b>Додаткове зображення №4</b></td>
			<td>
				<div class="file-upload">
					<button type="button">Завантажити файл4</button>
					<input type="file" name="uploadfile4">
				</div>
				<input type="text" id="filename4" class="filename" disabled>
			</td>
		</tr>
		<tr>
			<td><b>Додати новину</b></td>
			<td>
				<input type='submit' value="Додати">
			</td>
		</tr>
	</table>
	</form>
	<br><br>

	</div>
{/if}

<!-- DELETE NEWS -->

{if !empty($action) && $action == 'delete'}
	<div class="fixed">
	Видалити цю новину?
	<form action="/{$lang_url}/admin/delete/1/" method="POST">
		<input type="hidden" name="id" value="{$delete_id}" />
	<input type='submit'  value="Так">
	</form>
	</div>
{/if}

<!-- EDIT AND SAVE NEWS -->

{if !empty($action) && $action == 'edit'}
   <div class="fixed">
	   <h2 style="text-align: left;">Редагування новини</h2><br>
		   <form action="/{$lang_url}/admin/save/" method="POST" enctype=multipart/form-data>
			   <input type="hidden" name="edit_id" value="{$edit_id}">
			   <table border='1' width='100%'>
				   <tr>
					   <td><b>Назва новини</b></td>
					   <td>
						   <textarea name="new_news_name">{$edit_title}</textarea>
					   </td>
				   </tr>
				   <tr>
					   <td><b>Текст новини</b></td>
					   <td>
						   <textarea name="new_news_text">{$edit_text}</textarea>
					   </td>
				   </tr>
				   <tr>
					   <td><b>Відео новини</b></td>
					   <td>
						   <textarea name="new_news_video">{$edit_video}</textarea>
					   </td>
				   </tr>
				   <tr>
					   <td><b>Зображення новини</b></td>
                           <td>
                           <textarea name="new_news_img">{$edit_img}</textarea>
                           </td>
				   </tr>
				   <tr>
					   <td><b>Додаткове зображення №2</b></td>
					   <td>
						   <textarea name="new_news_img2">{$edit_img2}</textarea>
					   </td>
				   </tr>
				   <tr>
					   <td><b>Додаткове зображення №3</b></td>
					   <td>
						   <textarea name="new_news_img3">{$edit_img3}</textarea>
					   </td>
				   </tr>
				   <tr>
					   <td><b>Додаткове зображення №4</b></td>
					   <td>
						   <textarea name="new_news_img4">{$edit_img4}</textarea>
					   </td>
				   </tr>
				   <!-- IMAGE ADDITIONAL EDITING -->
				    <tr>
					   <td><b>Яку фотографію Ви хочете змінити?</b></td>
					   <td>
					   	<div class="change-photo">
						   <form action="/{$lang_url}/admin/save/" method="post" enctype=multipart/form-data>
							   <p><input name="foto" type="radio" value="1">Зображення новини1</p>
							   <p><input name="foto" type="radio" value="2">Додаткове зображення №2</p>
							   <p><input name="foto" type="radio" value="3">Додаткове зображення №3</p>
							   <p><input name="foto" type="radio" value="4">Додаткове зображення №4</p>
							   <div class="file-upload">
									<button type="button">Завантажити файл</button>
									<input type="file" name="new_uploadfile">
								</div><input type="text" id="new_uploadfile" class="filename-change-photo" disabled><br>
					           <input type="submit" value="Зберегти нову фотографію"><br>
							   <input type="hidden" name="edit_id" value="{$edit_id}">
	  				 </form></div>
					   </td>
				   </tr>
				   <!--  / IMAGE ADDITIONAL EDITING -->
				   <tr>
					   <td><b>Зберегти правки</b></td>
					   <td>
						   <input type='submit' value="Зберегти правки">

					   </td>
				   </tr>
		   </form>

			   </table>
	<br><br>
   </div>
{/if}

{if !empty($action) && $action == 'profileslist'}


<div class="fixed adminTable">
	<br>
	<h2 style="text-align: left;">Наші підопічні</h2><br>
	<a href='/{$lang_url}/admin/addprofile/'>ДОДАТИ ПІДОПІЧНОГО</a>
	<br>
	<table border='1' width='100%'>
		<tr>
			<td><b>ID</b></td>
			<td><b>Ф І О</b></td>
			<td><b>МЕТА</b></td>
			<td><b>Керування</b></td>
		</tr>
		{foreach from=$s_profiles_list item=profile}
		<tr>
			<td>{$profile.id}</td>
			<td>{$profile.fio}</td>
      <td>{$profile.purpose}</td>
			<td>
				<a href='/{$lang_url}/admin/editprofile/{$profile.id}/'>редагувати</a> &nbsp;&nbsp;&nbsp;
				<a href='/{$lang_url}/admin/deleteprofile/{$profile.id}/'>видалити</a>
			</td>
		</tr>
		{/foreach}
	</table>
	<br><br>
</div>
{/if}
