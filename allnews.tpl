<style>
    .small-news-text img {
        max-width: 100%;
        height: auto;
        width: 35%;
        float: left;
        max-height: 70px;
        vertical-align: top;
        margin: 0 10px 0px 0;
    }
</style>

<div class="fixed">
    <br>
    <h2 style="text-align: left;">Всi новини</h2><br>

    <div class="tabs">
                <div class="over">
                    <form class="subscribe-block">
                        <div>
                            <div>ПІДПИШИСЬ НА РОЗСИЛКУ</div>
                            <div class="subscribe">
                                <input type="text" placeholder="Введіть свій E-mail" />
                                <input type="submit" value="ПІДПИСАТИСЯ" />
                            </div>
                        </div>
                    </form>
                    <div class="news-title">
                        <div>НОВИНИ</div>
                        <ul class="bookmark">
                            <li>останнi</li>
                            <li>популярнi</li>
                        </ul>
                    </div>
                </div>
                <div class="bookmarker-box">
                    <ul class="small-news-list">
                        {foreach from=$posts item=post}
                        <li>
                            <h3 class="small-news-title"><a href="/ru/news/get/{$post.id}/">{$post.title}</a></h3>
                            <div class="small-news-text"><img src='{$post.img}'>{$post.text}...</div>
                            <div class="small-news-date">{$post.date}</div>
                        </li>
                        {/foreach}
                    </ul>
                </div>
    </div>
                <div class="pagination">
                       {section name=page start=1 loop=$s_pages+1 step=1}
                    <form action="/{$lang_url}/allnews/" method="post">
			   <input type="hidden" name="{$smarty.section.page.index}" value="">
                           <input type='submit' value="{$smarty.section.page.index}">
                    </form>
                          {/section}
            </div>
</div>
