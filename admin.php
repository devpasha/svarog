<?php

// LOGIN
if (!empty($action) && $action == 'login'){

    function adminCheck(){
        if  ($_POST["login"] =="" && $_POST["password"] == ""){
            return true;
        }
        else {
            return false;
          }
    }
    if(adminCheck()){
        $_SESSION['key'] = "asd";
        header('Location: /ua/admin/list/');
        }
    else{
        $wrong = "Невірний пароль";
        $smarty->assign('$s_wrong', $wrong);
        }

    $smarty -> assign('action', $action);
    $smarty -> assign('title', 'Адміністративна частина сайту Благодійна Організація "Фонд "Сварог"');
    $smarty -> display("site/body/header.tpl");
    $smarty -> display("site/pages/".basename(__FILE__, '.php').".tpl");
    $smarty -> display("site/body/footer.tpl");

   }
   // NEWS LIST

   if (!empty($action) && $action == 'list') {

       if($_SESSION["key"] == "asd") {

       // GET ALL NEWS

       $news_list = $sql -> getAll("select * from Posts order by published_date desc");

       // TO SMARTY ARRAY

       $smarty -> assign('action', $action);
       $smarty -> assign('smarty_news_list', $news_list);

       $smarty -> assign('title', 'Адміністративна частина сайту Благодійна Організація "Фонд "Сварог"');
       $smarty -> display("site/body/header.tpl");
       $smarty -> display("site/pages/".basename(__FILE__, '.php').".tpl");
       $smarty -> display("site/body/footer.tpl");

       }
   }
   // ADD NEWS

   if (!empty($action) && $action == 'add') {

       if($_SESSION["key"] == "asd") {

       $smarty -> assign('action', $action);
       $smarty -> assign('title', 'Добавление новости');
       $smarty -> display("site/body/header.tpl");
       $smarty -> display("site/pages/".basename(__FILE__, '.php').".tpl");
       $smarty -> display("site/body/footer.tpl");
       }
   }

   // SUBMIT TO DATABASE

   if (!empty($action) && $action == 'submit') {

       @$news_name = stripvuln($_POST['news_name']);
       @$news_text = stripvuln($_POST['news_text']);
       @$news_video = stripvuln($_POST['news_video']);

       @$news_name = addslashes($news_name);
       @$news_text = addslashes($news_text);

       $uploaddir = '/var/www/sport19/data/www/fond-svarog.com.ua/data/html/images/blog/';

       if ($news_name && $news_text && $_FILES['uploadfile']) {

           $uploadfile = $uploaddir.basename($_FILES['uploadfile']['name']);
           $imageName = basename($uploadfile);
           copy($_FILES['uploadfile']['tmp_name'], $uploadfile);
           $imgLink = "/data/html/images/blog/$imageName";
           $imgLink = addslashes($imgLink);


           if (!empty($_FILES['uploadfile2']['name'])) {
               $uploadfile2 = $uploaddir . basename($_FILES['uploadfile2']['name']);
               $imageName2 = basename($uploadfile2);
               copy($_FILES['uploadfile2']['tmp_name'], $uploadfile2);
               $imgLink2 = "/data/html/images/blog/$imageName2";
               $imgLink2 = addslashes($imgLink2);
           }
           if (!empty($_FILES['uploadfile3']['name'])) {
               $uploadfile3 = $uploaddir . basename($_FILES['uploadfile3']['name']);
               $imageName3 = basename($uploadfile3);
               copy($_FILES['uploadfile3']['tmp_name'], $uploadfile3);
               $imgLink3 = "/data/html/images/blog/$imageName3";
               $imgLink3 = addslashes($imgLink3);
           }

           if (!empty($_FILES['uploadfile4']['name'])) {
               $uploadfile4 = $uploaddir . basename($_FILES['uploadfile4']['name']);
               $imageName4 = basename($uploadfile4);
               copy($_FILES['uploadfile4']['tmp_name'], $uploadfile4);
               $imgLink4 = "/data/html/images/blog/$imageName4";
               $imgLink4 = addslashes($imgLink4);
           }

           $sql -> exec("insert into Posts(`title`, `text`, `img`, `img2`, `img3`, `img4`, `video`, `published_date`) values('".$news_name."', '".$news_text."', '".$imgLink."', '".$imgLink2."', '".$imgLink3."', '".$imgLink4."', '".$news_video."', NOW())");
           header('Location: /ru/admin/list/');
       }
       else {
            die("Ошибка! Размер фотографии слишком большой или формат не поддерживается или есть незаполненые поля");
            }

   }

   // DELETE NEWS

   if (!empty($action) && $action == 'delete') {

       if($_SESSION["key"] == "asd") {

       $smarty -> assign('delete_id', $id);

       if(!empty($_POST['id'])) {

           $sql -> exec("DELETE from Posts WHERE id = ".$id." ");
           header('Location: /ua/admin/list/');
       }

       $smarty -> assign('action', $action);
       $smarty -> display("site/body/header.tpl");
       $smarty -> display("site/pages/".basename(__FILE__, '.php').".tpl");
       $smarty -> display("site/body/footer.tpl");

       }
     }
     // EDIT NEWS

     if (!empty($action) && $action == 'edit') {

         if($_SESSION["key"] == "asd") {


         $smarty -> assign('edit_id', $id);
         $title = $sql->getAll("select TITLE from Posts where id = $id");
         $title = $title[0]['TITLE'];
         $smarty -> assign('edit_title', $title);

         $text = $sql->getAll("select text from Posts where id = $id");
         $text = $text[0]['text'];
         $smarty -> assign('edit_text', $text);

         $video = $sql->getAll("select video from Posts where id = $id");
         $video = $video[0]['video'];
         $smarty -> assign('edit_video', $video);

         $img = $sql->getAll("select img from Posts where id = $id");
         $img = $img[0]['img'];
         $smarty -> assign('edit_img', $img);

         $img2 = $sql->getAll("select img2 from Posts where id = $id");
         $img2 = $img2[0]['img2'];
         $smarty->assign('edit_img2', $img2);

         $img3 = $sql->getAll("select img3 from Posts where id = $id");
         $img3 = $img3[0]['img3'];
         $smarty->assign('edit_img3', $img3);

         $img4 = $sql->getAll("select img4 from Posts where id = $id");
         $img4 = $img4[0]['img4'];
         $smarty->assign('edit_img4', $img4);

         $smarty -> assign('action', $action);
         $smarty -> display("site/body/header.tpl");
         $smarty -> display("site/pages/".basename(__FILE__, '.php').".tpl");
         $smarty -> display("site/body/footer.tpl");
         }
      }
      // SAVE NEWS
      if (!empty($action) && $action == 'save') {

        if(!empty($_POST['new_news_name'])) {

            @$new_news_name=$_POST['new_news_name'];
            @$new_news_text=$_POST['new_news_text'];
            @$edit_id=$_POST['edit_id'];
            @$new_news_video=$_POST['new_news_video'];
            @$edit_img=$_POST['new_news_img'];
            @$edit_img2=$_POST['new_news_img2'];
            @$edit_img3=$_POST['new_news_img3'];
            @$edit_img4=$_POST['new_news_img4'];

            $new_news_name = addslashes($new_news_name);
            $new_news_text = addslashes($new_news_text);

          $sql -> exec("update Posts set title = '".$new_news_name."', text = '".$new_news_text."', img = '".$edit_img."', img2 = '".$edit_img2."', img3 = '".$edit_img3."', img4 = '".$edit_img4."', video = '".$new_news_video."' where id = '".$edit_id."' ");
         header('Location: /ua/admin/list/');
      }

      // Addition for  EDIT Images
      if (!empty($_POST['foto']) && !empty($_FILES['new_uploadfile']['name'])){ // если выбран переключатель и загружено изображение
          @$edit_id=$_POST['edit_id'];
          $index = $_POST['foto']; // индекс изображения, которое будем менять
          $uploaddir = '/var/www/sport19/data/www/fond-svarog.com.ua/data/html/images/blog/';
          $new_uploadfile = $uploaddir.basename($_FILES['new_uploadfile']['name']);
          $new_image_name = basename($new_uploadfile);
          copy($_FILES['new_uploadfile']['tmp_name'], $new_uploadfile);
          $new_img_link = "/data/html/images/blog/$new_image_name";
          $new_img_link = addslashes($new_img_link);
          if ($index == 1){
              $sql -> exec("UPDATE Posts set img = '' where id = '".$edit_id."' "); // удаляем старое изображение
              $sql -> exec("UPDATE Posts set img = '".$new_img_link."' where id = '".$edit_id."' "); // прописываем новое
          }
          if ($index == 2){
              $sql -> exec("UPDATE Posts set img2 = '' where id = '".$edit_id."' ");
              $sql -> exec("UPDATE Posts set img2 = '".$new_img_link."' where id = '".$edit_id."' ");
          }
          if ($index == 3){
              $sql -> exec("UPDATE Posts set img3 = '' where id = '".$edit_id."' ");
              $sql -> exec("UPDATE Posts set img3 = '".$new_img_link."' where id = '".$edit_id."' ");
          }
          if ($index == 4){
              $sql -> exec("UPDATE Posts set img4 = '' where id = '".$edit_id."' ");
              $sql -> exec("UPDATE Posts set img4 = '".$new_img_link."' where id = '".$edit_id."' ");
          }
          header("Location: /ua/admin/edit/$edit_id/");
      }

      $smarty -> assign('action', $action);
      $smarty -> display("site/body/header.tpl");
      $smarty -> display("site/pages/".basename(__FILE__, '.php').".tpl");
      $smarty -> display("site/body/footer.tpl");
  }

  // PROFILES LIST

  if (!empty($action) && $action == 'profileslist') {

      if($_SESSION["key"] == "asd") {

      // GET ALL PROFILES
      $profiles_list = $sql -> getAll("select * from children order by date desc");

      // TO SMARTY ARRAY

      $smarty -> assign('action', $action);
      $smarty -> assign('s_profiles_list', $profiles_list);

      $smarty -> assign('action', $action);
      $smarty -> assign('title', 'Новий підопічний');
      $smarty -> display("site/body/header.tpl");
      $smarty -> display("site/pages/".basename(__FILE__, '.php').".tpl");
      $smarty -> display("site/body/footer.tpl");
      }
  }
