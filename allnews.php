<?php

// (Пагинация)   
    //подсчитываем общее количество строк в таблице на данный момент
    $rows = $sql->getAll('select id from `Posts`');  
    $rows = $sql->getAll('SELECT FOUND_ROWS()');   
    $rowsQuantity = $rows[0]["FOUND_ROWS()"]; // заносим его в переменную rowsQuantity. (массив двухмерный - по этому такая сложная формула)
    //print_r($rowsQuantity);
    
    // определяем сколько страниц потребуется, что бы вывести все новости, по 40 новостей на страницу и заносим его в smarty.
    $pages = ceil($rowsQuantity / NEWS_PER_PAGE );
    $smarty->assign('s_pages', $pages);
    
    // задаем начальную строку и лимит вывода. Дальше эти значения будут автоматически пересчитываться в зависимости от того, кнопка какой страницы была нажата
       $begin = 0;
       $quantity = 40;
       
      if(empty($_POST)){           
      $news_list = $sql -> getAll("select * from Posts order by published_date desc limit $begin, $quantity");  
        }
        else {
        $coefficient = key($_POST);
        $begin = ($coefficient-1) * 40;
        $news_list = $sql -> getAll("select * from Posts order by published_date desc limit $begin, $quantity"); 
        }       
// (/Пагинация)

  foreach ($news_list as $news) {
    
    $date = explode(" ", $news['published_date']);
    $date = $date[0];
    $text = implode(' ', array_slice(explode(' ', $news['text']), 0, 30)); 
    
    $news_array[] = array(
        'title' => $news['title'],
        'date' => $date,
        'text' => $text,
        'id' => $news['id'],
        'img' => $news['img']
    );
    
}

$smarty -> assign('posts', $news_array);
$smarty -> assign('title', 'Всі новини');
$smarty -> display("site/body/header.tpl");
$smarty -> display("site/pages/".basename(__FILE__, '.php').".tpl");
$smarty -> display("site/body/footer.tpl");