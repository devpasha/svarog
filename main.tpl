<div class="flexslider">
            <ul class="slides">
                <li>
                    <img src="{$s_last_news[0].img}" alt=""/>
                    <div class="slider-info">
                        <div>
                            <h3 class="slider-title">{$s_last_news[0].title}</h3>
                            <a class="slider-more" href="/ua/news/get/{$s_last_news[0].id}/">ДЕТАЛЬНІШЕ</a>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="{$s_last_news[1].img}" alt=""/>
                    <div class="slider-info">
                        <div>
                            <h3 class="slider-title">{$s_last_news[1].title}</h3>
                            <a class="slider-more" href="/ua/news/get/{$s_last_news[1].id}/">ДЕТАЛЬНІШЕ</a>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="{$s_last_news[2].img}" alt=""/>
                    <div class="slider-info">
                        <div>
                            <h3 class="slider-title">{$s_last_news[2].title}</h3>
                            <a class="slider-more" href="/ua/news/get/{$s_last_news[2].id}/">ДЕТАЛЬНІШЕ</a>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="{$s_last_news[3].img}" height="650px" width="auto" alt=""/>
                    <div class="slider-info">
                        <div>
                            <h3 class="slider-title">{$s_last_news[3].title}</h3>
                            <a class="slider-more" href="/ua/news/get/{$s_last_news[3].id}/">ДЕТАЛЬНІШЕ</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="fixed">
            <div class="about-us">
                <div class="fixed-2">
                    <h3 class="about-us-title">Ми заснували Благодійну Організацію "Фонд "Сварог" для підтримки всіх верств населення, для покращення життя кожного із НАС, і залучення всіх не байдужих до благодійної діяльності та допомоги ближнім.</h3>
                    <div class="about-us-text">Фонд ставить на меті розвивати такі основні сфери життя як: спорт,  медицина та освіта. Фонд допоможе ВАМ вести активний та здоровий спосіб життя, постійно займатися спортом та знаходити час для нових захоплень, що збільшують наш кругозір, пізнавати нові знання, допоможе покращити стан освіти та медицини по всій Україні.</div>
                    <div>
                        <div class="soc-block">
                            <a class="soc-fb" href="https://www.facebook.com/fondsvarog/" target="_blank">Ми на Facebook</a>
                            <a class="soc-inst" href="https://www.instagram.com/fondsvarog/?hl=ru" target="_blank">Ми в instagram</a>
                            <a class="soc-yt" href="https://www.youtube.com/channel/UCCaZiPuhoJV9H8x3xSPv5hg" target="_blank">Ми на youtube</a>
                            <a class="soc-tw" href="https://twitter.com/fondsvarog" target="_blank">Ми в twitter</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="join-block">
            <div class="fixed-2">
                <div class="join-over">
                    <p>Приєднуйся, якщо хочєш стати частиною команди.</p>
                    {*<a href="/" class="join-link">ПРИЄДНАТИСЯ</a>*}
                </div>
            </div>
        </div>
        <div class="fixed">
            <ul class="project-list">
                {foreach from=$allposts item=spost}
                    {if $spost.id == 86}
                        <li>
                            <div class="project-pic"><img src="/data/html/images/project-pic_1.jpg" width="1140" height="174" alt=""/></div>
                            <div class="project-text">{$spost.title}</div>
                            <a class="project-more" href="/ru/news/get/{$spost.id}/">ДЕТАЛЬНІШЕ</a>
                        </li>
                    {/if}
                {/foreach}
                {foreach from=$allposts item=spost}
                    {if $spost.id == 90}
                        <li>
                            <div class="project-pic"><img src="/data/html/images/project-pic_2.jpg" width="1140" height="174" alt=""/></div>
                            <div class="project-text">{$spost.title}</div>
                            <a class="project-more" href="/ru/news/get/{$spost.id}/">ДЕТАЛЬНІШЕ</a>
                        </li>
                    {/if}
                {/foreach}
                {foreach from=$allposts item=spost}
                    {if $spost.id == 132}
                        <li>
                            <div class="project-pic"><img src="/data/html/images/project-pic_3.jpg" width="1140" height="174" alt=""/></div>
                            <div class="project-text">{$spost.title}</div>
                            <a class="project-more" href="/ru/news/get/{$spost.id}/">ДЕТАЛЬНІШЕ</a>
                        </li>
                    {/if}
                {/foreach}
                {foreach from=$allposts item=spost}
                    {if $spost.id == 91}
                        <li>
                            <div class="project-pic"><img src="/data/html/images/project-pic_3.jpg" width="1140" height="174" alt=""/></div>
                            <div class="project-text">{$spost.title}</div>
                            <a class="project-more" href="/ru/news/get/{$spost.id}/">ДЕТАЛЬНІШЕ</a>
                        </li>
                    {/if}
                {/foreach}
                <li>
                    <div class="project-pic"><img src="/data/html/images/project-pic_5.jpg" width="1140" height="174" alt=""/></div>
                    <div class="project-text">БАЖАЄТЕ СТАТИ ВОЛОНТЕРОМ ФОНДУ?<span>Зв'яжіться з нами, і ми обговоримо ваші пропозиції</span></div>
                    <a class="project-more" href="/ua/volunteer/">СТАТИ ВОЛОНТЕРОМ</a>
                </li>
            </ul>
        </div>
        <div class="fixed">
            <div class="tabs">
                <div class="over">
                    <form class="subscribe-block" action="/ua/" method="post">
                        <div>
                            <div>ПІДПИШИСЬ НА РОЗСИЛКУ</div>
                            <div class="subscribe">
                               <input type="email" name="email" placeholder="Введіть свій E-mail" />
                               <input type="submit" value="ПІДПИСАТИСЯ" />
                            </div>
                        </div>
                    </form>
                    <div class="news-title">
                        <div>НОВИНИ</div>
                        <ul class="bookmark">
                            <li>останнi</li>
                            <li>популярнi</li>
                        </ul>
                    </div>
                </div>
                <div class="bookmarker-box">
                    <ul class="small-news-list">
                        {foreach from=$posts item=post}
                        <li>
                            <h3 class="small-news-title"><a href="/ru/news/get/{$post.id}/">{$post.title}</a></h3>
                            <div class="small-news-text"><img src='{$post.img}'>{$post.text}...</div>
                            <div class="small-news-date">{$post.date}</div>
                        </li>
                        {/foreach}
                    </ul>
                </div>
            </div>
        </div>
        <div class="donate-block" style="display:none">
            <div class="fixed_3">
                <h3 class="donate-title">ПІДТРИМАЙТЕ  <span>ПРОЕКТ</span></h3>
                <div class="donate">
                    <div class="donate-info"><span>ЗІБРАНО <b>13004 грн</b></span></div>
                    <div class="donate-info"><span>ЦІЛЬ <b>60000 грн</b></span></div>
                    <a class="donate-link" href="/">Фондувати</a>
                </div>
            </div>
        </div>
